Hi! I'm the Engineering Manager for the [Security Insights](https://handbook.gitlab.com/handbook/engineering/development/sec/security-risk-management/security-insights/) group within our Sec Section.  We build amazing tools to assist with vulnerability management.

I'm an avid basketball player and you can find me at most Denver Nuggets games. In addition to software development, I enjoy 3D printing, building small products and improving things around the house. 



#### Connect with me

<a href="https://www.linkedin.com/in/neilmccorrison/"><img align="left" src="https://img.shields.io/badge/LinkedIn-0A66C2?&style=for-the-badge&logo=LinkedIn&logoColor=white" /></a>
<a href="mailto:nmccorrison@gitlab.com"><img align="left" src="https://img.shields.io/badge/Email-EA4335?&style=for-the-badge&logo=Gmail&logoColor=white" /></a>
<a href="https://calendly.com/nmccorrison-gitlab"><img align="left" src="https://img.shields.io/badge/Schedule a Meeting-4285F4?&style=for-the-badge&logo=Google Calendar&logoColor=white" /></a>


